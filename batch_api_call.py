import urllib2, sys
import requests
import json

# authentication information
auth_id = 'cfb706df-d8bb-41f0-8cfc-d3a7e5f2666a'
auth_token = 'CIOdIJj69bB3vrnSSMEF'
params = {'auth-id' : auth_id, 'auth-token' : auth_token}
headers = {'content-type': 'application/json', 'host' : 'api.smartystreets.com'}
url = 'https://api.smartystreets.com/street-address'

# read from standard in
i = 1
data = []
for line in sys.stdin:
    address = {}
    # parse pipe separated file from standard in
    parsed = line.split("|")
    address['input_id'] = parsed[0]
    address['street'] = parsed[1]
    if parsed[2] != '':
        address['secondary'] = parsed[2]
    address['city'] = parsed[3]
    address['state'] = parsed[4]
    address['zipcode'] = parsed[5]
    # send batch every 100 records
    data.append(address)
    if i % 100 == 0:
        r = requests.post(url, headers = headers, params = params, data = json.dumps(data))
        print json.dumps(r.json())
        data = []
        sys.stderr.write('Processing Record: ' + str(i) + "\n")
    i = i + 1

# send last batch
r = requests.post(url, headers = headers, params = params, data = json.dumps(data))
print json.sumps(r.json())
sys.stderr.write('Finished processing ' + str(i) + 'records.' + '\n')
