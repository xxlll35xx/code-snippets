#Example of logging in python.
#Example comes from: http://pingbacks.wordpress.com/2010/12/21/python-logging-tutorial/
#Logging formatters can be found at: http://docs.python.org/2/howto/logging.html
#Logging LogRecord attributes can be found at: http://docs.python.org/2/howto/logging.html

import logging

log = logging.getLogger('plane1')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
handler_stream = logging.StreamHandler()
handler_stream.setFormatter(formatter)
handler_stream.setLevel(logging.ERROR)
log.addHandler(handler_stream)
handler_file = logging.FileHandler('plane.log')
handler_file.setFormatter(formatter)
log.addHandler(handler_file)

def fly():
    log.debug('All systems operational')
    log.info('Airspeed 300 knots')
    log.warn('Low on fuel')
    log.error('No fuel. Trying to glide.')
    log.critical('Glide attempt failed. About to crash.')

if __name__ == '__main__':
	fly()
