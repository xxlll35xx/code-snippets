nstall R from Source
# from
# http://files.meetup.com/11316072/R-on-the%20RaspberryPi-3.pdf

# Fetch dependencies
sudo apt-get install gfortran libreadline6-dev libx11-dev libxt-dev
libpng-dev libjpeg-dev libcairo2-dev xvfb
# Fetch latest R source code
mkdir R_HOME && cd R_HOME
wget http://cran.rstudio.com/src/base/R-3/R-3.2.5.tar.gz && tar zxvf R 3.2.5.tar.gz
# Build R from source (takes about an hour)
cd R-3.2.5
./configure --with-cairo --with-jpeglib && make && sudo make install
# alias R and Rscript commands to run R executables in R_HOME
