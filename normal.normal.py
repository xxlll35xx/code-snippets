import numpy as np
import pymc3 as pm
import matplotlib.pyplot as plt
%matplotlib inline

# real data
y = np.array([64.94, 36.81, 36.81, 13.03, 16.31, 24.77, 16.70, 81.96, 17.29, \
				76.97, 84.97, 85.99, 61.21, 98.97, 98.97, 107.12, 91.56, 97.21, \
				106.20, 86.17, 147.04])


niter = 1000
with pm.Model() as model:
    # define priors
    mu = pm.Normal('mu', mu=100, sd=1000)
    #sigma = pm.Normal('sigma', mu=np.std(y), sd = 1000)
    sigma = pm.Gamma("sigma", alpha=10, beta=1)

    # define likelihood
    y_obs = pm.Normal('Y_obs', mu=mu, sd=sigma, observed=y)

    # inference
    start = pm.find_MAP()
    step = pm.Slice()
    trace = pm.sample(niter, step, start, random_seed=123, progressbar=True)

pm.traceplot(trace);
pm.plot_posterior(trace);