#!/usr/bin/env python

import pyautogui
import time

# wait 5 seconds between pyautgui invocations
pyautogui.PAUSE = 5


# do not crash program when cursors
# is placed in upper left corner
pyautogui.FAILSAFE = False

width, height = pyautogui.size()

try:
	while True:
		pyautogui.moveTo(0, 0, duration=1)
		pyautogui.moveTo(width, 0, duration=1)
		pyautogui.moveTo(width, height, duration=1)
		pyautogui.moveTo(0, height, duration=1)
		time.sleep(60)
except KeyboardInterrupt: 
	print("\nDone\n")