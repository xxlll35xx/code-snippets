#--------------------------------------------------
# R version of Vincent Granville's Perl program
# chaos and cluster
# 
# Ported by: C.Ortega - 2013-04-25
# Algorithm described at:
# http://www.analyticbridge.com/profiles/blogs/from-chaos-to-clusters-statistical-modeling-without-models?goback=%2Egde_4737538_member_235464603#%21
#--------------------------------------------------

#---------------distance - function -----------
distance <- function(x,y,p,q) {
  dist <- abs(x-p) + abs(y-q)
  if( dist < 0.04 && dist >0 ) { weight <- 1} else { weight <- 0.05 * exp(-20 * dist)}
  return(weight)
}

#-------------  MAIN PROGRAM ------------------
  
n <- 100 #dark matter
m <- 500 #visible matter
niter <- 200

fixed_x <- 0; fixed_y <- 0
for(k in 1:n) {
  fixed_x[k] <- 2 * runif(1) - 0.5
  fixed_y[k] <- 2 * runif(1) - 0.5
}

init_x <- 0; init_y <- 0
moving_x <- 0;  moving_y <- 0
rebirth <- 0; d2init <- 0;  d2last <- 0
for(k in 1:m) {
  init_x[k] <- runif(1)
  init_y[k] <- runif(1)
  moving_x[k] <- init_x[k]
  moving_y[k] <- init_y[k]
  rebirth[k] <-0
  d2init[k] <-0
  d2last[k] <-0
}

tmp_x <- 0 ; tmp_y <- 0
delta <- 0
to_R <- 0
for(iteration in 1:niter) {
  
  for(k in 1:m) {
    
    x <- moving_x[k]
    y <- moving_y[k]
    
    new_x <- 0
    new_y <- 0
    sum_weight <- 0
    
    for(l in 1:m) {
      
      p <- moving_x[l]
      q <- moving_y[l]
    
      weight <- distance(x,y,p,q)
      if(k==l) {weight <- 0}
      
      new_x <- new_x + weight * p
      new_y <- new_y + weight * q 
      sum_weight <- sum_weight + weight
      
    }
    
    for(l in 1:n) {
      p <- fixed_x[l]
      q <- fixed_y[l]
      weight <- distance(x,y,p,q)
      new_x <- new_x + weight * p
      new_y <- new_y + weight * q
      sum_weight <- sum_weight + weight
    }
    
   new_x <- new_x / sum_weight  
   new_y <- new_y / sum_weight  
   new_x <- new_x + 0.10 * (runif(1)-0.50)
   new_y <- new_y + 0.10 * (runif(1)-0.50)
   tmp_x[k] <- new_x   
   tmp_y[k] <- new_y   
    
   if(runif(1) < 0.1/(1+iteration)) {
     tmp_x[k] <- runif(1)
     tmp_y[k] <- runif(1)
     rebirth[k] <- 1
   }
    
  }
  
  delta[iteration] <- 0
  for(k in 1:m) {
    delta[iteration] <- delta[iteration] + abs(moving_x[k]-tmp_x[k]) +
abs(moving_y[k]-tmp_y[k])
    d2init[k] <- abs(moving_x[k] - init_x[k]) + abs(moving_y[k] - init_y[k])
    d2last[k] <- abs(moving_x[k] - tmp_x[k]) + abs(moving_y[k] - tmp_y[k])
    moving_x[k] <- tmp_x[k]
    moving_y[k] <- tmp_y[k]
  }

  delta[iteration] <- delta[iteration]/m
  
iter_val <- rep(iteration,m)  
to_R_tmp <- cbind(iter_val, moving_x, moving_y, rebirth, d2init, d2last)
to_R <- rbind(to_R, to_R_tmp)
  
} #for(iteration...

to_R <- as.data.frame(to_R[2:dim(to_R)[1],])
colnames(to_R) <- NULL
rownames(to_R) <- NULL
names(to_R) <- c('iter','x','y','new','d2init','d2last')

save(to_R, file="chaoscluster.RData")
#load("chaoscluster.RData")

#----------------------------------------
# Plot
#----------------------------------------
vv <- to_R
iter <- to_R$iter

for (n in 1:200) {
  x<-vv$x[iter == n] 
  y<-vv$y[iter == n] 
  z<-vv$new[iter == n]
  plot(x,y,xlim=c(0,1),ylim=c(0,1),pch=20,col=1+z,xlab="",ylab="",axes=FALSE,
main=paste(n))
}

#---------------End of program-----------------  