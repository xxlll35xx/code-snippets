#import rpy2 library
import rpy2.robjects as robjects

#submit R code
robjects.r('''
library(RJDBC)
drv <- JDBC(driverClass = "net.sourceforge.jtds.jdbc.Driver",
            classPath = "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar")

conn <- dbConnect(drv, "jdbc:jtds:sqlserver://bva.bvops.net;instance=bvanalysis;domain=BANCVUE;appName=RazorSQL;ssl=request;useCursors=true",
                  "Leland.Lockhart","Vgy7.cft")

query <- paste("SELECT
  *
FROM
        ESRI_Demographics.dbo.CFY12_01_ZP;", sep = "")

results <- dbGetQuery(conn, query)
''')

#save data to python object
data = robjects.r('results')

#list column names in dataframe
data.colnames
data.names


#turn column into a list
city = list(data[1])

#turn string vector of numbers into a list of numbers
population = [int(a) for a in data[3]]

#convert vector into numpy array
import numpy as np

arr = np.array(data[3]).astype('int64')




