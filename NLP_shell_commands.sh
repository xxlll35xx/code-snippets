# Common shell commands for text processing
# With annotation

#download works of shakespeare
curl http://www.gutenberg.org/cache/epub/100/pg100.txt > shakes.txt

# The tr tool takes every instance of the first string and maps it to the second string.
# -c command takes the complement of the first sequence
# In this example, tr takes everything that isn't an alphabetic character and turns it into
# a newline character.
# The -s command simply squeezes multiple instances of the second character into a single instance.
# This is done after the mapping of string1 to string2.
# In this example, I've piped the output through 'less' to prevent the output from 
# streaming continuously.
tr -sc 'A-Za-z' '\n' < shakes.txt | less

# From here, we can sort the words alphabetically.
tr -sc 'A-Za-z' '\n' < shakes.txt | sort | less

# With this sorted list, we can use 'uniq' to display only unique words.
# The '-c' command counts occurences of each unique word.
tr -sc 'A-Za-z' '\n' < shakes.txt | sort | uniq -c | less

# This list is sorted alphabetically. 'sort' lets us sort the list by frequency.
# Here, the '-n' tells sort to sort according to a string numerical value, 
# and '-r' tells sort reverse order the results, (i.e., to order the results from
# high to low).
tr -sc 'A-Za-z' '\n' < shakes.txt | sort | uniq -c | sort -n -r | less

# Notice that some words appear twice.  This is because 'And' and 'and' are treated
# as different words.  To fix this, let's map all upper case letters to lower case letters.
# To do these, we pre-pend our previous commands with an instance of 'tr' command that
# maps upper-case letters to lower-case.  We then pipe this output through the rest of our
# existing sequence.
tr 'A-Z' 'a-z' < shakes.txt | tr -sc 'A-Za-z' '\n' | sort | uniq -c | sort -n -r | less

#########################
# STEMMING
#########################

# Let's now do some basic searching.  Let's look at all words ending in 'ing' in the text.
# In this line, we use grep to search for occurences of 'ing' at the end of a word.  The dollar
# sign restricts our search to word endings, as opposed to any 'ing' occurences.
tr 'A-Z' 'a-z' < shakes.txt | tr -sc 'A-Za-z' '\n' | grep 'ing$' | sort | uniq -c | sort -n -r| less

# This produces some words that should not be stemmed.  Let's only select words that contain at least
# one vowel in the syllable prior to the presence of 'ing'.  These words should be suitable for stemming.
# Below, '[aeiou]' selects any vowel, '.' means any character, and '*' means zero or more of the preceeding 
# symbol (in this case, any character).  In this we, we can select only words ending in 'ing' that have
# at least one previous vowel.
tr 'A-Z' 'a-z' < shakes.txt | tr -sc 'A-Za-z' '\n' | grep '[aeiou].*ing$' | sort | uniq -c | sort -n -r| less
