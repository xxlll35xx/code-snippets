#!/bin/bash

# Shell script to test mapper and reducer locally before running on Hadoop cluster.
# mapper.py and reducer.py result in canonical word count example.
# Assumes mapper.py and reducer.py are in the current working directory.

echo "foo foo quux labs foo bar quux" | ./mapper.py | sort -k1,1 | ./reducer.py

#reading all of Shakespeare's works
#curl http://www.gutenberg.org/cache/epub/100/pg100.txt | ./mapper.py |sort -k1,1 | ./reducer.py > output.tsv