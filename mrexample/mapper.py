#!/usr/bin/env python

# Python Word Count Mapper
# Emits (word,1) tuples

import sys
import re

#match to words; remove punctuation
match = r'(?u)\b\w+\b'

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = re.findall(match, line)
    # increase counters
    for word in words:
        # write the results to STDOUT (standard output);
        # what we output here will be the input for the
        # Reduce step, i.e. the input for reducer.py
        #
        # tab-delimited; the trivial word count is 1
        # map word to lowercase before emitting
        print '%s\t%s' % (word.lower(), 1)
