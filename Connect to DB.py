import pyodbc
import csv


connection_str = """
			Driver={SQL Server Native Client 10.0};

			Server=servername;

			Database=dbname;

			Trusted_Connection=yes;

"""

db_connection = pyodbc.connect(connection_str)

db_connection.autocommit = True

db_cursor = db_connection.cursor()

sql_command = """
			select * from table;

			"""

db_cursor.execute(sql_command)

cols = []

for row in db_cursor.columns(table = "table"):
	cols.append(row.column_name)

db_cursor.execute(sql_command)


with open('output.csv','w') as f:
	writer = csv.writer(f)
	writer.writerow(cols)
	for row in db_cursor:
		#print ("%s\t%s\t%s") % (row.BillingID, row.EndOfPeriod, row.ShortName)
		#writer.writerow([row.BillingID, row.EndOfPeriod,row.ShortName])
		writer.writerow(row)

