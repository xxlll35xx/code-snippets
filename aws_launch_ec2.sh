AMI=ami-00ddb0e5626798373
INSTANCE_TYPE=t2.micro
SECURITY_GROUP_ID=sg-0c4d1d2124e428ed6
SUBNET_ID=subnet-05aa747093308f1d7

aws ec2 run-instances --image-id $AMI --instance-type $INSTANCE_TYPE --key-name lll-personal-iam --security-group-ids $SECURITY_GROUP_ID --subnet-id $SUBNET_ID

aws ec2 describe-instances --filters "Name=instance-type,Values=t2.micro"
aws ec2 describe-instances --filters "Name=instance-type,Values=t2.micro" --query "Reservations[].Instances[].InstanceId"

aws ec2 terminate-instances --instance-ids i-07902dc9705716429