#!/bin/bash
# Install Vowpal Wabbit and dependencies on Mac OS X.
# Requires Homebrew.

brew install libtool
brew install automake
brew install boost
brew install git


#install vw at /usr/local/bin/vw
git clone https://github.com/JohnLangford/vowpal_wabbit.git
cd vowpal_wabbit
./autogen.sh
./configure
make
make install