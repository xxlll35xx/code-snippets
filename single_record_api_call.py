import sys
import json, urllib

base_url = 'https://api.smartystreets.com/street-address?'

for line in sys.stdin:
	address = {}
	parsed = line.split("|")
	address['street'] = parsed[0]
	if parsed[1] != '':
		address['secondary'] = parsed[1]
	address['city'] = parsed[2]
	address['state'] = parsed[3]
	address['zipcode'] = parsed[4]
	address['auth-id'] = 'cfb706df-d8bb-41f0-8cfc-d3a7e5f2666a'
	address['auth-token'] = 'CIOdIJj69bB3vrnSSMEF'
	street = urllib.urlencode(address)
	response = urllib.urlopen(base_url + street)
	data = json.loads(response.read())
	response.close()
	print json.dumps(data)
