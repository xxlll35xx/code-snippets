import os
from docx import Document
import re

QUERY = re.compile('pool|pool party|poolparty')

def read_docx(fpath):
	try:
		doc = Document(fpath)
	except:
		print(f'Cannot process {fpath}')
		return None
	return doc

def get_text(fpath):
	doc = read_docx(fpath)
	if doc:
		text = [paragraph.text.replace("\n"," ").strip() for paragraph in doc.paragraphs]
		return " ".join(text).strip().lower()
	else:
		return ''

def find_query(s):
	result = QUERY.findall(s)
	return result

docx = []
for subdir, dirs, files in os.walk('/Users/llockhart/Documents/'):
	for filename in files:
		filepath = subdir + os.sep + filename
		filepath = filepath.replace('//','/')

		if filepath.endswith('.docx') and "$" not in filepath:
			docx.append(filepath)

for doc in docx:
	text = get_text(doc)
	search_result = find_query(text)
	if search_result:
		print(f'{doc} -> {search_result}')

