import pyodbc
import pandas.io.sql as psql

connection_str = """
			Driver={SQL Server Native Client 10.0};

			Server=bva.bvops.net\\bvanalysis;

			Database=MSTR_Reporting;

			Trusted_Connection=yes;

"""

cnxn = pyodbc.connect(connection_str) 
cursor = cnxn.cursor()
sql = "SELECT * FROM TABLE"

df = psql.frame_query(sql, cnxn)
cnxn.close()