from sympy.solvers import solvers
from sympy import Symbol, symbols
from sympy import init_printing

init_printing()

I,P,NQ,Cap,BC,AC = symbols('I P NQ Cap BC AC')

nonqual_rate = P * (29*NQ/365)

below_cap_rate = Cap * (BC/365)

above_cap_rate = (P - Cap) * (AC/365)

equation = nonqual_rate + below_cap_rate + above_cap_rate - I

solve(equation,P)