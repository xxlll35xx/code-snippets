"""Converts xls or xlsx to csv.

Usage:  python xls_to_csv.py infile [outfile] [sheetname]
"""

import xlrd
import csv
import sys

nargs = len(sys.argv)

infile = sys.argv[1]

if nargs >= 3:
	outfile = sys.argv[2]
else:
	outfile = 'outfile.csv'

if sys.argv[3]:
	sheetname = sys.argv[3]
else:
	sheetname = 'Sheet1'

def csv_from_excel(infile = infile, outfile = outfile):
	wb = xlrd.open_workbook(infile)
	sh = wb.sheet_by_name(sheetname)
	your_csv_file = open(outfile, 'wb')
	wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

	for rownum in xrange(sh.nrows):
		wr.writerow(sh.row_values(rownum))

	your_csv_file.close()

if __name__ == '__main__':
	csv_from_excel(infile, outfile)
