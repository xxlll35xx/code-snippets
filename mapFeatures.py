import numpy as np

def mapFeatures(X1,X2,degree=2, include_original_features = True):
	"""Maps two input vectors to produce
	polnomial combinations equal to supplied
	degree.

	Returns a new Numpy array with original features
	plus newly created polynomial combinations. Quadratic
	features calculated by default.

	Ex.:  mapFeature(X1,X2,degree = 2) returns 
	numpy array with features X1, X2, X1 ^ 2, 
	X2 ^ 2, X1 * X2.

	Inputs:
	X1 and X2:  Numpy arrays with of dimensions
	n x 1.

	Degree:  Order of polynomial features to create.

	include_original_features:  boolean.  If True, include 
	original features in output.  True by default.
	"""

	if X1.shape != X2.shape:
		print "Error:  Vectors must be of the same length."
		return None

	if X1.shape[1] != 1 or X2.shape[1] != 1:
		print "Error:  Vectors must contain exactly one column."
		return None


	out = np.array([])

	for i in range(1,degree+1):
		for j in range(i+1):
			new = np.power(X1,(i-j)) * (np.power(X2,j))
			if out.any():
				out = np.hstack((out,new))
			else:
				out = new

	if not include_original_features:
		return out[:,2:]
	else:
		return out

if __name__ == '__main__':
	X1 = np.random.random((4,1))
	X2 = np.random.random((4,1))

	quadratic = mapFeatures(X1,X2,degree=2,include_original_features = True)

	print quadratic